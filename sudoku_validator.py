"""
@Author Marco A. gallegos
@Date   2021/01/11
@Update 2021/01/11
@Description
    Validador de un tablero de sudoku prellenado, solo validar cuando un tablero prellenado es valido o no
"""


def validate_rows(board:list)->bool:
    """Function to determinate the row state

    :param board: [description]
    :type board: list
    :return: [description]
    :rtype: bool
    """
    result = True

    for row in board:
        previous_numbers = []
        for element in row:
            if element in previous_numbers:
                return False
            if element != 0:
                previous_numbers.append(element)
    return result


def validate_cols(board:list)->bool:
    """Function to determinate the cols state

    :param board: [description]
    :type board: list
    :return: [description]
    :rtype: bool
    """
    result = True

    current_col = 0
    len_row = len(board[0])

    while current_col < len_row:
        col_previous_values = []
        for row in board:
            if row[current_col] in col_previous_values:
                return False
            if row[current_col] != 0:
                col_previous_values.append(row[current_col])
        current_col += 1

    return result


def validate_squares(board:list)->bool:
    result = True

    current_col = 0
    current_row = 0
    len_rows = len(board)
    len_cols = len(board[0])

    while current_row < len_rows:
        previous_values = []
        for i in range(current_row, current_row+3):
            if current_col < len_cols:
                #print("+"*20)
                for j in range(current_col, current_col+3):
                    #print(f"indexes i:{i} j:{j}")
                    if board[i][j] in  previous_values:
                        return False
                    if board[i][j] != 0:
                        previous_values.append(board[i][j])
        
        current_col += 3
        if current_col > len_cols:
            current_col =0
            current_row += 3

    return result


def main()->bool:
    board = [
        [1,0,0,9,0,0,8,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,4,0,0,0,0],
        [0,0,3,0,0,0,7,0,0],
        [0,0,0,0,0,0,6,0,0],
        [6,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0,2],
    ]

    has_good_rows = validate_rows(board=board)
    print(f"good rows : {has_good_rows}")

    has_good_cols = validate_cols(board=board)
    print(f"good cols : {has_good_cols}")
    
    has_good_squares = validate_squares(board=board)
    print(f"good square : {has_good_squares}")


    if has_good_cols and has_good_rows and has_good_squares:
        print("eres un crack BD")
        return True
    else:
        print('no eres un crack :"v')
        return False


if __name__ == '__main__':
    valid = main()

    print(valid)